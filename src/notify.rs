use std::process::Command;

pub fn notify(msg: &str) {
    Command::new("notify-send")
        .arg("-a")
        .arg("Dongim")
        .arg("Dongim")
        .arg(msg)
        .output()
        .expect("Error in sending notification");
}
