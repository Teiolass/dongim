use super::basic_types::*;
use super::logger::nlog;
use super::notify::notify;
use chrono::{DateTime, Duration, Local};

pub enum CurrentSession {
    Active(DateTime<Local>),
    Paused(Duration),
    Finished,
}

pub struct TimerState {
    pub session: CurrentSession,
    pub point_in_schedule: usize,
    pub mode: SessionMode,
    pub sessions: Vec<Session>,
}
impl TimerState {
    pub fn from_schedule(schedule: Vec<Session>) -> Self {
        let d = Duration::minutes(schedule[0].1.into());
        let m = schedule[0].0;
        let mm = match m {
            SessionMode::Work => "work",
            SessionMode::Pause => "pause",
        };
        nlog(&format!(
            "Advance session; now {} mins of {}.",
            d.num_minutes(),
            mm
        ));
        TimerState {
            session: CurrentSession::Paused(d),
            point_in_schedule: 0usize,
            mode: m,
            sessions: schedule,
        }
    }
}

pub fn time_label(state: &TimerState) -> String {
    use CurrentSession::*;
    if let Finished = state.session {
        String::from("Whoop")
    } else {
        let duration = match state.session {
            Active(time) => time.signed_duration_since(Local::now()),
            Paused(duration) => duration,
            Finished => panic!(),
        };
        duration_to_label(&duration)
    }
}

fn duration_to_label(duration: &Duration) -> String {
    format!(
        "{:02}:{:02}",
        duration.num_minutes(),
        duration.num_seconds() % 60
    )
}

/// This function returns true when the schedule is finished, false otherwise
pub fn advance_session(state: &mut TimerState, should_notify: bool) -> bool {
    state.point_in_schedule += 1;
    if state.point_in_schedule < state.sessions.len() {
        let d = Duration::minutes(state.sessions[state.point_in_schedule].1.into());
        state.session = CurrentSession::Paused(d);
        state.mode = state.sessions[state.point_in_schedule].0;
        let m = match state.mode {
            SessionMode::Work => "work",
            SessionMode::Pause => "pause",
        };
        nlog(&format!(
            "Advance session; now {} mins of {}.",
            d.num_minutes(),
            m
        ));
        let m = match state.mode {
            SessionMode::Work => "Back to work!",
            SessionMode::Pause => "Now relax.",
        };
        if should_notify {
            notify(&format!("Session finished. {}", m));
        }
        false
    } else {
        nlog("Schedule finished");
        if should_notify {
            notify("All done for this schedule.\n Good job!");
        }
        state.session = CurrentSession::Finished;
        true
    }
}

pub fn toggle_pause(state: &mut TimerState) {
    use CurrentSession::*;
    state.session = match state.session {
        Active(time) => {
            nlog("Timer is now freezed.");
            Paused(time.signed_duration_since(Local::now()))
        }
        Paused(duration) => {
            nlog("Timer is now running.");
            Active(Local::now().checked_add_signed(duration).unwrap())
        }
        Finished => Finished,
    };
}

pub fn check_finished(state: &TimerState) -> bool {
    use CurrentSession::*;
    match state.session {
        Active(time) => {
            let t = time.signed_duration_since(Local::now());
            t.num_seconds() <= 0
        }
        Paused(_duration) => {
            // duration.num_seconds() < 0
            false
        }
        Finished => false,
    }
}
