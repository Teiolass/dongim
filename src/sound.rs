use sdl2::mixer::{self, Channel, Chunk, Sdl2MixerContext};
use std::{ops::Deref, path::Path, sync::Arc, thread};

struct ThreadedChunk(Chunk);
impl Deref for ThreadedChunk {
    type Target = Chunk;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
unsafe impl Send for ThreadedChunk {}
unsafe impl Sync for ThreadedChunk {}

pub struct Sound {
    chunk: Arc<ThreadedChunk>,
}

impl Sound {
    // we ask a ref to SoundSystem because we cant create a chunk when
    // mixer is not initialized
    pub fn from_file<P>(_system: &SoundSystem, path: P) -> Self
    where
        P: AsRef<Path>,
    {
        let raw_chunk = Chunk::from_file(path).unwrap();
        let chunk = Arc::new(ThreadedChunk(raw_chunk));
        Sound { chunk }
    }

    pub fn play(&self, channel: Channel) {
        let new_chunk = self.chunk.clone();
        thread::spawn(move || {
            channel.play(&new_chunk, 0).unwrap();
        });
    }

    pub fn play_on_first_free(&self) {
        self.play(Channel(-1));
    }
}

pub struct SoundSystem {
    _context: Sdl2MixerContext,
}

impl SoundSystem {
    pub fn new() -> Self {
        let context = mixer::init(mixer::InitFlag::MP3).unwrap();
        // see advice: https://docs.rs/sdl2/latest/sdl2/mixer/fn.open_audio.html
        mixer::open_audio(44_100, mixer::DEFAULT_FORMAT, mixer::DEFAULT_CHANNELS, 1024).unwrap();
        mixer::allocate_channels(2);
        SoundSystem { _context: context }
    }
}
