use parser_macro::Configuration;
use std::fs;

use super::basic_types::*;
macro_rules! empty_config_type {
    ($conf:ty, $t:ty) => {
        impl ConfigurationField<$conf> for $t {
            fn get_field_mut<'a>(_config: &'a mut $conf, _name: &str) -> &'a mut $t {
                panic!();
            }
        }
    };
}

#[allow(dead_code)]
#[derive(Default, Configuration)]
pub struct Config {
    pub window_width: u32,
    pub window_height: u32,
    pub clock_text_size: u32,
    pub console_text_size: u32,
    pub bg_color: String,
    pub fg_work_color: String,
    pub fg_pause_color: String,
    pub schedule: Vec<Session>,
    pub font_timer: String,
    pub font_console: String,
}
empty_config_type!(Config, i32);
empty_config_type!(Config, f32);
empty_config_type!(Config, Vec<i32>);

#[derive(Copy, Clone)]
//#[derive(ConfigurationType)]
enum ConfigTypes {
    Int32,
    Uns32,
    Float32,
    Str,
    VecInt,
    VecSes,
}

fn assoc_type(t: &str) -> ConfigTypes {
    use ConfigTypes::*;
    match t {
        stringify!(i32) => Int32,
        stringify!(u32) => Uns32,
        stringify!(f32) => Float32,
        stringify!(String) => Str,
        stringify!(Vec<i32>) => VecInt,
        stringify!(Vec<Session>) => VecSes,
        x => {
            panic!("{}", x)
        }
    }
}

trait Configuration {
    type Associator; // @todo: maybe we should give a trait for the associator
    fn get_associator(&self) -> Self::Associator;
}

trait ConfigurationField<Conf: Configuration> {
    fn get_field_mut<'a>(conf: &'a mut Conf, name: &str) -> &'a mut Self;
}

enum ParsedLine {
    Normal,
    VecStart(String, ConfigTypes),
    VecEnd,
}
enum ParsingStatus {
    Normal,
    VecEl(String, ConfigTypes),
}

fn process_line(
    line: &str,
    config: &mut Config,
    associator: &<Config as Configuration>::Associator,
    status: &ParsingStatus,
) -> ParsedLine {
    // Remove comments
    let mut i = 0;
    let mut escaped = false;
    let mut in_string = false;
    for c in line.chars() {
        match c {
            '\\' => {
                escaped = true;
            }
            '\"' => {
                if !escaped {
                    in_string = !in_string;
                }
                escaped = false;
            }
            '#' => {
                if !escaped & !in_string {
                    break;
                }
                escaped = false;
            }
            _ => {
                escaped = false;
            }
        }
        i += 1;
    }
    let line = &line[0..i];
    // Handle empty lines
    let line = line.trim();
    if line.is_empty() {
        return ParsedLine::Normal;
    }
    // The real job
    match status {
        ParsingStatus::Normal => {
            let split = line.split_once('=');
            if let Some((lhs, rhs)) = split {
                let lhs = lhs.trim();
                let rhs = rhs.trim();
                for (name, conf_type) in associator {
                    if lhs == *name {
                        use ConfigTypes::*;
                        match conf_type {
                            Int32 => {
                                let val = rhs.parse::<i32>().unwrap();
                                let field =
                                    ConfigurationField::<Config>::get_field_mut(config, lhs);
                                *field = val;
                            }
                            Uns32 => {
                                let val = rhs.parse::<u32>().unwrap();
                                let field =
                                    ConfigurationField::<Config>::get_field_mut(config, lhs);
                                *field = val;
                            }
                            Float32 => {
                                let val = rhs.parse::<f32>().unwrap();
                                let field =
                                    ConfigurationField::<Config>::get_field_mut(config, lhs);
                                *field = val;
                            }
                            Str => {
                                let beg = rhs.find('"').unwrap();
                                let end = rhs.rfind('"').unwrap();
                                let val = rhs[beg + 1..end].to_string();
                                let field =
                                    ConfigurationField::<Config>::get_field_mut(config, lhs);
                                *field = val;
                            }
                            _ => {
                                panic!("We cannot assign arrays like this: {}", lhs);
                            }
                        }
                        return ParsedLine::Normal;
                    }
                }
                panic!("Name not found: {}.", lhs);
            }
            let beg = line.find('[').unwrap();
            let end = line.rfind(']').unwrap();
            let inside = &line[beg + 1..end];
            for (name, conf_type) in associator {
                if inside == *name {
                    return ParsedLine::VecStart(String::from(inside), *conf_type);
                }
            }
            panic!("Error 1");
        }
        ParsingStatus::VecEl(vec, cf_ty) => {
            use ConfigTypes::*;
            let beg = line.find('[');
            if beg != None {
                let beg = beg.unwrap();
                let end = line.rfind(']').unwrap();
                let inside = &line[beg + 1..end];
                if inside == vec {
                    return ParsedLine::VecEnd;
                } else {
                    panic!("wrong format");
                }
            }
            match cf_ty {
                VecInt => {
                    let val = line.parse::<i32>().expect(line);
                    let field: &mut Vec<i32> =
                        ConfigurationField::<Config>::get_field_mut(config, &vec);
                    field.push(val);
                }
                VecSes => {
                    let split = line.trim().split_once(' ').expect("wrong session format");
                    let (sx, dx) = split;
                    let mode;
                    if sx.find('w') != None {
                        mode = SessionMode::Work;
                    } else if sx.find('p') != None {
                        mode = SessionMode::Pause;
                    } else {
                        panic!();
                    }
                    let m = dx.trim().parse::<i32>().expect(line);
                    let val = (mode, m);
                    let field: &mut Vec<Session> =
                        ConfigurationField::<Config>::get_field_mut(config, &vec);
                    field.push(val);
                }
                _ => {
                    panic!("not a vector type")
                }
            }
            ParsedLine::VecStart(vec.to_string(), *cf_ty)
        }
    }
}

pub fn parse_configuration_file(file: &str) -> Config {
    let mut config = Config::default();
    let associator = config.get_associator();
    let contents = fs::read_to_string(file).unwrap();
    let bytes = contents.as_bytes();
    let mut current_beginning: usize = 0;
    let mut status = ParsingStatus::Normal;
    for (current_ending, &ch) in bytes.iter().enumerate() {
        if ch == b'\n' {
            let result = process_line(
                &contents[current_beginning..current_ending],
                &mut config,
                &associator,
                &status,
            );
            current_beginning = current_ending + 1;
            use ParsedLine::*;
            match result {
                Normal => {}
                VecStart(vec, ty) => {
                    status = ParsingStatus::VecEl(vec, ty);
                }
                VecEnd => {
                    status = ParsingStatus::Normal;
                }
            }
        }
    }
    config
}
