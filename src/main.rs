extern crate sdl2;
#[macro_use(defer)]
extern crate scopeguard;

use std::env;
use std::mem::take;

use sdl2::{
    event::Event,
    rect::Rect,
    render::{Texture, TextureCreator, WindowCanvas},
    ttf::Font,
    video::WindowContext,
};

mod basic_types;
mod logger;
mod notify;
mod palette;
mod parser;
mod sound;
mod timer;

use basic_types::*;
use logger::*;
use palette::Palette;
use sound::*;
use timer::*;

fn draw_timer<'a>(
    canvas: &mut WindowCanvas,
    timer_state: &TimerState,
    timer_env: &mut TimerEnv<'a>,
    general_style: &GeneralStyle,
    sdl_context: &SdlContext<'a>,
) {
    // text configuration
    let text = time_label(&timer_state);
    let color = match timer_state.mode {
        SessionMode::Work => Palette::FG_WORK,
        SessionMode::Pause => Palette::FG_PAUSE,
    };
    let text_style = TextStyle {
        color: color,
        font: timer_env.font,
    };
    draw_general_timer(
        canvas,
        &text,
        text_style,
        general_style,
        sdl_context,
        timer_env,
    );
}

struct TextStyle {
    color: usize,
    font: usize,
}
fn draw_general_timer<'a>(
    canvas: &mut WindowCanvas,
    text: &str,
    text_style: TextStyle,
    general_style: &GeneralStyle,
    sdl_context: &SdlContext<'a>,
    timer_env: &mut TimerEnv<'a>,
) {
    timer_env.text = general_style.fonts[text_style.font]
        .render(&text)
        .blended(general_style.palette[text_style.color])
        .unwrap()
        .as_texture(&sdl_context.texture_creator)
        .unwrap();
    let ws = sdl_context.window_size;
    let ts = {
        let query = timer_env.text.query();
        (query.width, query.height)
    };
    let x = (0.5 * (ws.0 - ts.0) as f32) as i32;
    let y = (0.5 * (ws.1 - ts.1) as f32) as i32;
    let rect = Rect::new(x, y, ts.0, ts.1);
    canvas.copy(&timer_env.text, None, Some(rect)).unwrap();
}

#[derive(Default)]
struct ConsoleState {
    is_active: bool,
    lines: Vec<String>,
    buffer: String,
    cursor_position: usize,
}

/// history_starting_height is measured in pixels
struct ConsoleEnv<'a> {
    font: usize,
    background_rectangle: Rect,
    text_textures: Vec<Texture<'a>>,
    bottom_line_height: f32,
    input_text_textures: Vec<Texture<'a>>,
    need_to_update_input: bool,
    history_starting_height: u32, // @todo use it as 1 unit = 1 line or implement half line scroll
}
impl Default for ConsoleEnv<'_> {
    fn default() -> Self {
        ConsoleEnv {
            font: 0,
            background_rectangle: Rect::new(0, 0, 0, 0),
            text_textures: Vec::new(),
            bottom_line_height: 0.0,
            input_text_textures: Vec::new(),
            need_to_update_input: false,
            history_starting_height: 0,
        }
    }
}

struct TimerEnv<'a> {
    text: Texture<'a>,
    font: usize,
    bell: Sound,
}

enum CursorMovement {
    Left,
    Right,
}

fn move_cursor(state: &mut ConsoleState, direction: CursorMovement) {
    let n = &mut state.cursor_position;
    match direction {
        CursorMovement::Left => {
            if *n > 0 {
                *n -= 1
            }
        }
        CursorMovement::Right => {
            if *n < state.buffer.len() {
                *n += 1
            }
        }
    }
}

fn toggle_console(console_state: &mut ConsoleState) {
    console_state.is_active = !console_state.is_active;
}

fn add_text_to_console<'a>(
    text: &str,
    general_style: &GeneralStyle,
    sdl_context: &SdlContext<'a>,
    console_state: &mut ConsoleState,
    console_env: &mut ConsoleEnv<'a>,
) {
    let text = if text.is_empty() { " " } else { &text };
    let surface = general_style.fonts[console_env.font]
        .render(text)
        .blended(general_style.palette[Palette::BG])
        .unwrap();
    let dtext = sdl_context
        .texture_creator
        .create_texture_from_surface(surface)
        .unwrap();
    console_state.lines.push(String::from(text));
    console_env.text_textures.push(dtext);
    console_state.cursor_position = 0;
}

fn push_buffer_into_command(state: &mut WholeState) {
    let txt = take(&mut state.console_state.buffer);
    let result = dispatch_commands(&txt, state);
    state.console_state.buffer = String::new();
    add_text_to_console(
        &txt,
        &state.general_style,
        &state.sdl_context,
        &mut state.console_state,
        &mut state.console_env,
    );
    use CommandParseError::*;
    match result {
        Ok(vec) => {
            for msg in vec {
                add_text_to_console(
                    &msg,
                    &state.general_style,
                    &state.sdl_context,
                    &mut state.console_state,
                    &mut state.console_env,
                );
            }
        }
        Err(EmptyCommand) => {}
        Err(CommandNotFound(name)) => {
            let msg = format!("Command not found: {}", &name);
            add_text_to_console(
                &msg,
                &state.general_style,
                &state.sdl_context,
                &mut state.console_state,
                &mut state.console_env,
            );
        }
        Err(BadArguments(msg)) => {
            add_text_to_console(
                &msg,
                &state.general_style,
                &state.sdl_context,
                &mut state.console_state,
                &mut state.console_env,
            );
        }
        Err(BadNumberArguments(expected, found)) => {
            let msg = format!("Expected {} arguments. Found {}.", expected, found);
            add_text_to_console(
                &msg,
                &state.general_style,
                &state.sdl_context,
                &mut state.console_state,
                &mut state.console_env,
            );
        }
    }
}

fn draw_console<'a>(
    canvas: &mut WindowCanvas,
    state: &ConsoleState,
    env: &mut ConsoleEnv<'a>,
    general_style: &GeneralStyle,
    sdl_context: &SdlContext<'a>,
) {
    canvas.set_draw_color(general_style.palette[Palette::BG_CONSOLE]);
    canvas.fill_rect(env.background_rectangle).unwrap();
    if env.need_to_update_input {
        env.input_text_textures = Vec::with_capacity(state.buffer.len());
        for (i, character) in state.buffer.chars().enumerate() {
            let color = if i == state.cursor_position {
                general_style.palette[Palette::FG_WORK]
            } else {
                general_style.palette[Palette::BG]
            };
            let texture = general_style.fonts[env.font]
                .render_char(character)
                .blended(color)
                .unwrap()
                .as_texture(sdl_context.texture_creator)
                .unwrap();
            env.input_text_textures.push(texture);
        }
    }
    let line_spacing = general_style.fonts[env.font].recommended_line_spacing();
    let offset_sx = line_spacing;
    let bottom = env.bottom_line_height as i32 - line_spacing;
    let mut current_x = offset_sx as u32;
    for (i, texture) in env.input_text_textures.iter().enumerate() {
        let texture_infos = texture.query();
        if i == state.cursor_position {
            canvas.set_draw_color(general_style.palette[Palette::BG]);
            canvas
                .fill_rect(Rect::new(
                    current_x as i32,
                    bottom,
                    texture_infos.width,
                    texture_infos.height,
                ))
                .unwrap();
        }
        canvas
            .copy(
                texture,
                None,
                Some(Rect::new(
                    current_x as i32,
                    bottom,
                    texture_infos.width,
                    texture_infos.height,
                )),
            )
            .unwrap();
        current_x += texture_infos.width;
    }
    if state.cursor_position == state.buffer.len() {
        let cursor_size = general_style.fonts[env.font].size_of_char('x').unwrap();
        canvas.set_draw_color(general_style.palette[Palette::BG]);
        canvas
            .fill_rect(Rect::new(
                current_x as i32,
                bottom,
                cursor_size.0,
                cursor_size.1,
            ))
            .unwrap();
    }
    let mut iterator = env.text_textures.iter_mut().rev();
    let first_text_index = env.history_starting_height / line_spacing as u32;
    for _ in 0..first_text_index {
        if let None = iterator.next() {
            break;
        }
    }
    const SEPARATOR_LINE_HEIGHT: u32 = 2;
    if first_text_index > 0 {
        let bottom = bottom - SEPARATOR_LINE_HEIGHT as i32;
        let screen_width = env.background_rectangle.width();
        let rect = Rect::new(0, bottom, screen_width, SEPARATOR_LINE_HEIGHT);
        canvas.set_draw_color(general_style.palette[Palette::BG]);
        canvas.fill_rect(rect).unwrap();
    }
    let bottom = bottom - line_spacing;
    for (i, text) in iterator.enumerate() {
        let current_y = bottom - line_spacing * i as i32;
        if current_y < -line_spacing {
            break;
        }
        let query = text.query();
        canvas
            .copy(
                text,
                None,
                Some(Rect::new(offset_sx, current_y, query.width, query.height)),
            )
            .unwrap();
    }
}

/// BadNumberArguments(u16, u16) is (expected, provided)
enum CommandParseError {
    EmptyCommand,
    CommandNotFound(String),
    BadNumberArguments(u16, u16),
    BadArguments(String),
}

fn dispatch_commands(
    command: &str,
    state: &mut WholeState,
) -> Result<Vec<String>, CommandParseError> {
    let mut splitted = command.trim().split(' ');
    let name = if let Some(name) = splitted.next() {
        name
    } else {
        return Err(CommandParseError::EmptyCommand);
    };
    match name {
        "toggle" => {
            nlog("Received toggle command");
            toggle_pause(&mut state.timer_state);
            Ok(vec![String::from("Toggling timer state")])
        }
        "advance" => {
            nlog("Received advance command");
            let res = advance_session(&mut state.timer_state, false);
            let ret = match res {
                true => "Advancing... Schedule finished",
                false => "Advancing one session",
            };
            Ok(vec![String::from(ret)])
        }
        "long" => Ok(vec![
            String::from("my"),
            String::from("string"),
            String::from("that"),
            String::from("covers"),
            String::from("mutliple"),
            String::from("lines"),
            String::from("and"),
            String::from("even"),
            String::from("more"),
        ]),
        "line" => {
            // @todo remove this silly command
            let n = {
                if let Some(x) = splitted.next() {
                    match x.parse::<u32>() {
                        Ok(n) => n,
                        Err(_) => {
                            let msg = String::from("line expected a uint arg");
                            return Err(CommandParseError::BadArguments(msg));
                        }
                    }
                } else {
                    return Err(CommandParseError::BadNumberArguments(1, 0));
                }
            };
            state.console_env.history_starting_height = n;
            Ok(vec![])
        }
        _ => Err(CommandParseError::CommandNotFound(String::from(name))),
    }
}

fn handle_timer_event(event: &Event, state: &mut WholeState) -> bool {
    use sdl2::event::Event::*;
    use sdl2::keyboard::Keycode::*;
    match event {
        KeyDown { keycode, .. } => match keycode {
            Some(Space) if !state.console_state.is_active => {
                toggle_pause(&mut state.timer_state);
                true
            }
            _ => false,
        },
        _ => false,
    }
}

/// returns true iff the event has been handled
fn handle_console_event(event: &Event, state: &mut WholeState) -> bool {
    use sdl2::event::Event::*;
    use sdl2::keyboard::Keycode::*;
    match event {
        KeyDown { keycode, .. } => match keycode {
            Some(Escape) => {
                toggle_console(&mut state.console_state);
                true
            }
            Some(Return) => {
                push_buffer_into_command(state);
                true
            }
            Some(Backspace) => {
                if state.console_state.is_active {
                    if state.console_state.cursor_position > 0 {
                        state
                            .console_state
                            .buffer
                            .remove(state.console_state.cursor_position - 1);
                    }
                    move_cursor(&mut state.console_state, CursorMovement::Left);
                }
                true
            }
            Some(Left) => {
                if state.console_state.is_active {
                    move_cursor(&mut state.console_state, CursorMovement::Left);
                }
                true
            }
            Some(Right) => {
                if state.console_state.is_active {
                    move_cursor(&mut state.console_state, CursorMovement::Right);
                }
                true
            }
            _ => false,
        },
        TextInput { text, .. } => {
            if state.console_state.is_active {
                for character in text.chars() {
                    if character.is_ascii_alphanumeric()
                        || character.is_ascii_whitespace()
                        || character.is_ascii_punctuation()
                    {
                        state.console_env.need_to_update_input = true;
                        state
                            .console_state
                            .buffer
                            .insert(state.console_state.cursor_position, character);
                        state.console_state.cursor_position += 1;
                    }
                }
            }
            true
        }
        MouseWheel { y, .. } => {
            let step = *y * 20;
            let x = state.console_env.history_starting_height as i32 + step;
            state.console_env.history_starting_height = if x > 0 { x as u32 } else { 0 };
            true
        }
        _ => false,
    }
}

enum FontEnum {
    TIMER = 0,
    CONSOLE = 1,
}

struct GeneralStyle<'a> {
    palette: Palette,
    fonts: Vec<Font<'a, 'a>>,
}

struct SdlContext<'a> {
    texture_creator: &'a TextureCreator<WindowContext>,
    window_size: (u32, u32),
}

struct WholeState<'a> {
    general_style: GeneralStyle<'a>,
    sdl_context: SdlContext<'a>,
    timer_state: TimerState,
    timer_env: TimerEnv<'a>,
    console_state: ConsoleState,
    console_env: ConsoleEnv<'a>,
    _sound_system: SoundSystem,
}

fn main() {
    // we are looking for the res folder in different locations depending
    // wether we are in debug or release mode
    #[cfg(debug_assertions)]
    let mut path = env::current_dir().unwrap();
    #[cfg(debug_assertions)]
    {
        path.push("res");
        path.push("Dummy");
    }
    #[cfg(not(debug_assertions))]
    let path = env::current_exe().unwrap();

    let mut conf_path = path.clone();
    conf_path.set_file_name("config.dong");
    let config = parser::parse_configuration_file(conf_path.to_str().unwrap());

    let mut log_path = path.clone();
    log_path.set_file_name("log.donglog");
    initialize_logger(log_path.to_str().unwrap());
    defer! {
        close_logger();
    }

    let mut bell_path = path.clone();
    bell_path.set_file_name("bell.wav");
    let mut font_timer_path = path.clone();
    font_timer_path.set_file_name(&config.font_timer);
    let mut font_console_path = path;
    font_console_path.set_file_name(&config.font_console);

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
    let mut event_pump = sdl_context.event_pump().unwrap();
    let window = video_subsystem
        .window("Dongim", config.window_width, config.window_height)
        .position(1300, 150)
        .build()
        .unwrap();
    let window_size = window.size();
    let mut canvas = window.into_canvas().build().unwrap();
    let texture_creator = canvas.texture_creator();

    let ttf_context = sdl2::ttf::init().unwrap();
    let font_timer = ttf_context
        .load_font(
            &font_timer_path,
            config.clock_text_size as u16, // @todo wrong type
        )
        .expect(&format!("Error while loading font in {}", font_timer_path.display()));
    let font_console = ttf_context
        .load_font(
            font_console_path,
            config.console_text_size as u16, // @todo wrong type
        )
        .unwrap();

    let palette = palette::Palette::from_config(&config);

    let mut state = {
        let sound_system = SoundSystem::new();

        let schedule = config.schedule.clone(); // @todo mhhhh
        let timer_state = TimerState::from_schedule(schedule);
        let timer_env = {
            let text = font_timer
                .render("00:00")
                .blended(palette[Palette::FG_WORK])
                .unwrap()
                .as_texture(&texture_creator)
                .unwrap();
            let bell = Sound::from_file(&sound_system, bell_path);
            TimerEnv {
                text,
                font: FontEnum::TIMER as usize,
                bell,
            }
        };
        let console_state = ConsoleState::default();
        let console_env = {
            let height = (window_size.1 as f32 * 0.75) as u32;
            let rect = Rect::new(0, 0, window_size.0, height);
            ConsoleEnv {
                background_rectangle: rect,
                font: FontEnum::CONSOLE as usize,
                bottom_line_height: height as f32,
                need_to_update_input: true,
                ..ConsoleEnv::default()
            }
        };
        let sdl_context = SdlContext {
            texture_creator: &texture_creator,
            window_size,
        };
        let fonts = vec![font_timer, font_console]; // @robustness be carefull when updating!
        let general_style = GeneralStyle { fonts, palette };
        WholeState {
            general_style,
            sdl_context,
            timer_state,
            timer_env,
            console_state,
            console_env,
            _sound_system: sound_system,
        }
    };

    'main_loop: loop {
        ::std::thread::sleep(std::time::Duration::new(0, 1_000_000_000_u32 / 60));
        if check_finished(&state.timer_state) {
            state.timer_env.bell.play_on_first_free();
            advance_session(&mut state.timer_state, true);
        }
        for event in event_pump.poll_iter() {
            use sdl2::event::Event::*;
            let handled = match event {
                Quit { .. } => {
                    break 'main_loop;
                }
                _ => false,
            };
            if handled {
                continue;
            }
            let handled = handle_timer_event(&event, &mut state);
            if handled {
                continue;
            }
            let _handled = handle_console_event(&event, &mut state);
        }
        canvas.set_draw_color(state.general_style.palette[Palette::BG]);
        canvas.clear();
        draw_timer(
            &mut canvas,
            &state.timer_state,
            &mut state.timer_env,
            &state.general_style,
            &state.sdl_context,
        );
        if state.console_state.is_active {
            draw_console(
                &mut canvas,
                &state.console_state,
                &mut state.console_env,
                &state.general_style,
                &state.sdl_context,
            );
        }
        canvas.present();
    }
    nlog("Closing");
}
