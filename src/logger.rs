use chrono::Local;
use std::fs::File;
use std::io::Write;

#[derive(Clone, Copy)]
pub enum LogLevel {
    Normal,
}

#[allow(non_upper_case_globals)]
static mut log_file: Option<File> = None;

pub fn initialize_logger(file: &str) {
    let version = env!("CARGO_PKG_VERSION");
    unsafe {
        let mut file = std::fs::OpenOptions::new()
            .append(true)
            .create(true)
            .open(file)
            .unwrap();
        file.write_all(b"\n").unwrap();
        let msg = &format!("[INIT] Initializing logger v{}\n", version);
        file.write_all(msg.as_bytes()).unwrap();
        log_file = Some(file);
    }
    log("", LogLevel::Normal);
}

pub fn close_logger() {
    unsafe {
        log_file = None;
    }
}

fn format_time() -> String {
    let dt = Local::now();
    dt.format("%H:%M:%S - %Y-%m-%d - %a").to_string()
}

pub fn log(msg: &str, level: LogLevel) {
    unsafe {
        let mut file = (&log_file).as_ref().unwrap();
        let head = match level {
            LogLevel::Normal => "[    ]",
        };
        let time = &format_time();
        let msg = format!("{} [{}]  {}\n", head, time, msg);
        file.write_all(msg.as_bytes()).unwrap();
    }
}
pub fn nlog(msg: &str) {
    log(msg, LogLevel::Normal)
}
