#[derive(Copy, Clone)]
pub enum SessionMode {
    Work,
    Pause,
}
pub type Session = (SessionMode, i32);
