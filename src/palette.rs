use super::parser::Config;
use sdl2::pixels::Color;
use std::ops::Index;

// @todo robustness - This function doesnt handle bad format well
// @todo clean - Can I run this function at compile time?
fn decode_hex(s: &str) -> Color {
    let r = u8::from_str_radix(&s[1..=2], 16).unwrap();
    let g = u8::from_str_radix(&s[3..=4], 16).unwrap();
    let b = u8::from_str_radix(&s[5..=6], 16).unwrap();
    Color { r, g, b, a: 255 }
}

pub struct Palette(Vec<Color>);

impl Palette {
    pub fn from_config(conf: &Config) -> Self {
        let fg_work = decode_hex(&conf.fg_work_color);
        let fg_pause = decode_hex(&conf.fg_pause_color);
        let bg = decode_hex(&conf.bg_color);
        let vec = vec![fg_work, fg_pause, bg];
        Palette(vec)
    }
    pub const FG_WORK: usize = 0;
    pub const FG_PAUSE: usize = 1;
    pub const BG: usize = 2;
    pub const BG_CONSOLE: usize = 0;
}
impl Index<usize> for Palette {
    type Output = Color;
    fn index(&self, index: usize) -> &Self::Output {
        &(self.0)[index]
    }
}
