use proc_macro::TokenStream;
use quote::{format_ident, quote};
use std::collections::HashMap;
use syn::parse_macro_input;

#[proc_macro_derive(Configuration)]
pub fn derive_config(item: TokenStream) -> TokenStream {
    let input = parse_macro_input!(item as syn::DeriveInput);
    let struct_name = format_ident!("{}", input.ident.to_string());
    let mut num: usize = 0;
    let mut members = quote!();
    let mut cont = HashMap::<(String, Option<(String, String)>), Vec<String>>::new();
    let data_struct = match input.data {
        syn::Data::Struct(xxx) => xxx,
        _ => panic!(),
    };
    let fields = match data_struct.fields {
        syn::Fields::Named(xxx) => xxx,
        _ => panic!(),
    };
    for field in fields.named {
        num += 1;
        let name = field.ident.as_ref().unwrap().to_string();
        let ty;
        match field.ty {
            syn::Type::Path(xxx) => ty = xxx,
            _ => panic!(),
        }
        let mut iterator_type = ty.path.segments.iter();
        let seg = iterator_type.next().unwrap();
        let mut ty_name = seg.ident.to_string();
        let mut arg = None;
        if ty_name == "Vec" {
            let args = match &seg.arguments {
                syn::PathArguments::AngleBracketed(xxx) => xxx,
                _ => panic!(),
            };
            let ty_path = match args.args.iter().next().unwrap() {
                syn::GenericArgument::Type(syn::Type::Path(xxx)) => xxx,
                _ => panic!(),
            };
            let nm = ty_path.path.segments.iter().next().unwrap().ident.to_string();
            let t = format!("{} < {} >", ty_name, nm);
            arg = Some((ty_name.clone(), nm));
            ty_name = t;
        }
        let key = (ty_name.clone(), arg);
        members = quote!(#members (String::from(#name), assoc_type(#ty_name)),);
        match cont.get_mut(&key) {
            None    => { cont.insert(key, vec![name]); }
            Some(v) => v.push(name),
        }
    }
    let mut fun = quote!();
    for ((tt, arg), v) in cont {
        let mut pfun = quote!();
        let type_id;
        if let Some((a, b)) = arg {
            let a = format_ident!("{}", a);
            let b = format_ident!("{}", b);
            type_id = quote!(#a<#b>);
        } else {
            let a = format_ident!("{}", tt);
            type_id = quote!(#a);
        }
        for nm in v {
            let ide = format_ident!("{}", nm);
            pfun = quote!(
                #pfun
                #nm => {
                    &mut config.#ide
                }
            );
        }
        fun = quote!( #fun
            impl ConfigurationField<#struct_name> for #type_id {
                fn get_field_mut <'a> (config: &'a mut #struct_name, name: &str
                                      ) -> &'a mut #type_id {
                    match name { #pfun
                        _ => {panic!();}
                    }
                }
            }
        );
    }
    let gen = quote!(
        impl Configuration for #struct_name {
            type Associator = [(String, ConfigTypes); #num];
            fn get_associator(self: &Self) -> Self::Associator { [#members] }
        }
        #fun
    );
    // println!("{}", gen);
    gen.into()
}

